# README #

### What is this repository for? ###

#### The Goal ####

To create a small program that retrieves a list of posts from a compendium (Oracle Marketing Cloud) feed and displays the 9 most recent posts to the user. To see it in action, visit [sheltered-mesa](http://sheltered-mesa-3605.herokuapp.com/). 

Display should include:

Title,
Author Name,
Publish Date,
Post Body

#### Other Notes ####

- Users should be required to enter a feed id.
- Users should be able to (optionally) provide search terms
- This can be a web-app.  This can be a command-line application.  This can be a GUI.  The programm SHOULD run on osx or linux so we can test it.


#### API Docs ####

API docs can be found here:

http://docs.oracle.com/cloud/latest/marketingcs_gs/OMCBC/#API/Resources/Feed.htm#Feed_GET_index_parent_publishers

One example url for the feed API is:

http://app.compendium.com/api/publishers/78f4b458-2c97-4c9a-bb88-f8124db03b21/feed


#### A few Valid Feed Ids ####

b69420d0-1ded-4e4f-a9bd-81fbc1063f1a
137f578c-65c0-42b5-8440-19bb7f99b035
78f4b458-2c97-4c9a-bb88-f8124db03b21

#### Responsive ####

The site is responsive, so do not hesitate to try it on your mobile devices (I would love to know if you have issues). 

* Version
* 1.0.0

### License ###

This project is licensed under the MIT License.
This means you can use and modify it for free in private or commercial projects.

### Framework Used ###

I have used a heavily modified version of [MINI](https://github.com/panique/mini) as the basis for this project. MINI is an extremely barebones PHP MVC framework. It is super easy to get started on, and I would definitely recommend it for POC projects such as this.

### How do I get set up? ###

* This project will require the AMP(Apache MySQL PHP) stack to run. (Technically, you can skip MySQL as we do not use it here. Though most dev environments do have a MySQL instance running)
* There are many online guides to set up your M(ac)AMP/L(inux)AMP environments. I particularly like [this](https://echo.co/blog/os-x-1010-yosemite-local-development-environment-apache-php-and-mysql-homebrew) for my Mac setup. Untested, but the #2 result on google looks promising for ubuntu users: [Ubuntu Setup](http://howtoubuntu.org/how-to-install-lamp-on-ubuntu).
* [Composure](https://getcomposer.org/doc/00-intro.md) is a requirement for this project.
* Dependencies are managed through composer. Clone/copy the contents of this repo into your local environment, and then run `composer install`. This will automatically resolve your dependencies. 
* There is no database configuration required.
* You will need to update your virtual hosts/hosts file to point to the location of the directory and give it a host name (say oracle.dev). 
* Point your browser to this URL and you should be good to go.
* As of now, there is no test coverage, though I hope to have some written soon. 
* There is an instance of the project running on Heroku at [sheltered-mesa](http://sheltered-mesa-3605.herokuapp.com/). 
* To get your instance running on Heroku, refer to this [guide](https://devcenter.heroku.com/articles/getting-started-with-php#introduction).

### Remaining Tasks ###
* Implement AJAX functionality for the app. There is a very rudimentary version commented out in the JS file that could form the basis of the AJAX app.
* Write Unit Tests.
* Write validation for $_POST varaibles

### Contribution guidelines ###

* Writing tests
* Code review
* Bug Reports

### Who do I talk to? ###

* Shiva Ray (shiva.ray@gmail.com)