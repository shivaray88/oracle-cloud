<div class="error container">
  <h1>Oops!</h1>
  <p>You have landed on the Error page. This might have happened for a couple of reasons:</p>
  <div>
    <ul>
      <li>You might land here when a page (= controller / method) does not exist.</li>
      <li>You might have entered an invalid feed ID. Check the help text to get some valid feeds!</li>
      <li>The API might be having issues. Please wait for some time before trying again.</li>
    </ul>
  </div>
</div>
