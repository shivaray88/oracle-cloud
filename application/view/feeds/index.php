<div class="container">
  <!-- add feed request form -->
  <h2>Get a Feed</h2>
  <form action="<?php echo URL; ?>feeds" method="POST">
    <div class = "form-element">
      <label>Feed ID</label>
      <input type="text" name="feedID" value="<?php echo isset($_POST['feedID']) ? $_POST['feedID'] : ''; ?>" required />
    </div>
    <div class = "form-element">
      <label>Number of feeds</label>
      <input type="number" name="count" value="<?php echo isset($_POST['count']) ? $_POST['count'] : '9'; ?>" required />
    </div>
    <div class = "form-element">
      <input type="submit" name="submit_feed_request" value="Submit" />
    </div>
  </form>
</div>
