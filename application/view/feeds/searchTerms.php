<div class="container">
  <!-- add feed request form -->
  <h2>Get a Feed</h2>
  <form action="<?php echo URL; ?>feeds/searchTerms" method="POST">
    <div class = "form-element">
      <label>Feed ID</label>
      <input type="text" name="feedID" value="<?php echo isset($_POST['feedID']) ? $_POST['feedID'] : ''; ?>" required />
    </div>
    <div class = "form-element">
      <label>Number of feeds</label>
      <input type="text" name="count" value="<?php echo isset($_POST['count']) ? $_POST['count'] : '9'; ?>" required />
    </div>
    <div class = "form-element">
      <label>Search Terms</label>
      <input type="text" name="searchTerms" value="<?php echo isset($_POST['searchTerms']) ? $_POST['searchTerms'] : ''; ?>" required />
    </div>
    <div class = "form-element">
      <input type="submit" name="submit_feed_request" value="Submit" />
    </div>
  </form>
</div>