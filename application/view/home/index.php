<div class="home container">
    <h2>Welcome!</h2>
    <p>Get started by clicking on any of the links on the top of the page! They are described below:</p>
    <h3>Home</h3>
    <p>You are here now. This is where the fun begins! </p>
    <h3>Feeds</h3>
    <p>This option lets you enter a feed ID and get a certain count of posts from that feed.</p>
    <h3>Search Terms</h3>
    <p>Same as feeds but lets you specify additional search terms as well.</p>
  <p><em>NOTE: You can always click the "Toggle help on/off" button for help in these pages</em></p>
</div>
