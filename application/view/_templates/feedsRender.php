<!-- main content output -->
<?php if(count($feeds) > 0){
  // @NOTE: uncomment the following line for debugging only.
  // Kint::dump($feeds);
?>
  <div class="container clearfix">
    <h2>Results:</h2>
    <?php
    $counter = 0;
    foreach ($feeds as $feed) {
      $counter++;
    ?>
      <div class = "feed box clearfix">
        <div class="feed-image">
          <img src="<?php echo (isset($feed->featured_image) ? $feed->featured_image : 'img/no-image.png'); ?>" />
        </div>
        <div class="feed-details">
          <h2><?php echo $feed->title; ?></h2>
          <?php if (isset($feed->web_url)){
            echo "<p>Web URL: <a href = '$feed->web_url' target=\"_blank\">Click Here</a> </p>";
          } ?>
          <?php if (isset($feed->author->name)){
            echo "<p>By: {$feed->author->name} </p>";
          } ?>
          <?php if (isset($feed->publish_date)){
            echo "<p>Publish Date: $feed->publish_date </p>";
          } ?>
          <?php if (isset($feed->asset_url)){
            echo "<p>Asset URL: <a href = '$feed->asset_url' target=\"_blank\">Click Here</a> </p>";
          } ?>
          <?php if (isset($feed->categories) && count($feed->categories) > 0) { ?>
            <p class = "clearfix"><span class = 'category-label'>Categories:</span>
              <?php foreach ($feed->categories as $cat) {
                echo "<span class = 'category' >$cat->name</span>";
              }?>
            </p>
          <?php } ?>
          <p>Content Type: <?php echo $feed->content_type->name; ?></p>
        </div>
      </div>
      <?php if (isset($feed->asset_url) &&
      !empty($body = Model::getPostBody($feed->asset_url)->body)){ ?>
        <div class="feed-body-toggler container" count = "<?php echo $counter; ?>">Display/hide Post Body</div>
        <div class = "feed-body" count = "<?php echo $counter; ?>">
        <?php echo $body; ?>
      </div>
    <?php }
    } ?>
  </div>
<?php } ?>
