<!DOCTYPE html>
<html lang="en">
<head>
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <meta charset="utf-8">
    <title>Marketing Cloud Demo</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- JS -->
    <!-- please note: The JavaScript files are loaded in the footer to speed up page construction -->
    <!-- See more here: http://stackoverflow.com/q/2105327/1114320 -->

    <!-- CSS -->
    <link href="<?php echo URL; ?>css/style.css" rel="stylesheet">
</head>
<body>
  <div class="page-wrapper">
    <!-- logo -->
    <a href="<?php echo URL; ?>">
      <div class="logo clearfix">
        <div class = "logo-img"><img src="<?php echo URL; ?>img/oracle.png" alt = "oracle-logo"/></div>
        <div class = "logo-text"><span>FEED DEMO</span></div>
      </div>
    </a>

    <!-- navigation -->
    <div class="navigation">
      <a href="<?php echo URL; ?>">home</a>
      <a href="<?php echo URL; ?>feeds">feeds</a>
      <a href="<?php echo URL; ?>feeds/searchTerms">Search Terms</a>
    </div>
