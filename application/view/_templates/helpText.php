<div class = "help-toggler container"><em>Toggle help on/off</em></div>
<div class="container help">
  <h2>Help</h2>
  <p>
    <em>
      Some sample feeds to test are (click to select, and then copy to your clipboard):
      <ul>
        <li>137f578c-65c0-42b5-8440-19bb7f99b035</li>
        <li>b69420d0-1ded-4e4f-a9bd-81fbc1063f1a</li>
        <li>78f4b458-2c97-4c9a-bb88-f8124db03b21</li>
      </ul>
      Some sample search terms are are:
      <ul>
        <li>blue moon</li>
        <li>green</li>
      </ul>
    </em>
  </p>
</div>