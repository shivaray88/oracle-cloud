<?php

/**
 * Class Songs
 * This is a demo class.
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Feeds extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/feeds/index
     */
    public function index() {
        // getting all songs and amount of songs
        //$feeds = $this->model->getPosts('b69420d0-1ded-4e4f-a9bd-81fbc1063f1a', 9);
        $feeds = array();
        if(isset($_POST["feedID"]) && isset($_POST["count"])){
            $feeds = $this->model->getPosts($_POST["feedID"], $_POST["count"]);
        }

        // load views. within the views we can echo out $songs and $amount_of_songs easily
        require APP . 'view/_templates/header.php';
        require APP . 'view/feeds/index.php';
        require APP . 'view/_templates/helpText.php';
        require APP . 'view/_templates/feedsRender.php';
        require APP . 'view/_templates/footer.php';
    }
    public function ajaxReturnFeed()
    {
        $feeds = $this->model->getPosts($_POST["feedID"], $_POST["count"]);
        echo $feeds;
    }

    /**
     * The only difference between index() and searchTerms() is that we can use the optional
     * parameter of search_terms here.
     */
    public function searchTerms(){
        $feeds = array();
        if(isset($_POST["feedID"]) && isset($_POST["count"]) && isset($_POST["searchTerms"])){
            $feeds = $this->model->getSearchPosts($_POST["feedID"], $_POST["count"], $_POST["searchTerms"]);
        }
        // load views. within the views we can echo out $songs and $amount_of_songs easily
        require APP . 'view/_templates/header.php';
        require APP . 'view/feeds/searchTerms.php';
        require APP . 'view/_templates/helpText.php';
        require APP . 'view/_templates/feedsRender.php';
        require APP . 'view/_templates/footer.php';
    }
}
