<?php

class Model {
  /**
   * Tries to reach the API, but redirects to error page in case of an issue
   * @param $url
   * @return \Psr\Http\Message\ResponseInterface
   */
  private function tryGuzzle($url){
    try{
      $client = new GuzzleHttp\Client();
      $res = $client->get($url);
      return $res;
    }catch (Exception $e){
      header('location: ' . URL . 'error');
    }
    return NULL;
  }
  /**
   * Get $count number of posts for a particular $feedID
   * @param $feedID
   * @param $count
   * @return mixed
   */
  public function getPosts($feedID, $count) {
    $res = $this->tryGuzzle(API.$feedID."/feed?page_size=$count");
    $posts = json_decode($res->getBody());
    return $posts;
  }

  /**
   * Get $count number of posts for a particular $feedID with  $searchTerms
   * @param $feedID
   * @param $count
   * @param $searchTerms
   * @return mixed
   */
  public function getSearchPosts($feedID, $count, $searchTerms) {
    $res = $this->tryGuzzle(API.$feedID."/feed?page_size=$count&search_terms=$searchTerms");
    $posts = json_decode($res->getBody());
    return $posts;
  }

  /**
   * Get the body from a URL
   * @param $bodyURL
   * @return mixed
   */
    public static function getPostBody($bodyURL){
      $client = new GuzzleHttp\Client();
      $res = $client->get($bodyURL);
      $body = json_decode($res->getBody());
      return $body;
    }
}