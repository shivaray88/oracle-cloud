//$(document).ready(function() {
//
//    // process the form
//    $('form').submit(function(event) {
//
//        // get the form data
//        // there are many ways to get this data using jQuery (you can use the class or id also)
//        var formData = {
//            'feedID'              : $('input[name=feedID]').val(),
//            'count'             : $('input[name=count]').val(),
//        };
//
//        // process the form
//        $.ajax({
//                type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
//                url         : 'feeds/ajaxReturnFeed', // the url where we want to POST
//                data        : formData, // our data object
//                dataType    : 'json', // what type of data do we expect back from the server
//                encode          : true
//            })
//            // using the done promise callback
//            .done(function(data) {
//
//                // log data to the console so we can see
//                console.log(data);
//
//                // here we will handle errors and validation messages
//            });
//
//        // stop the form from submitting the normal way and refreshing the page
//        event.preventDefault();
//    });
//
//});

$(document).ready(function() {
    $('.help-toggler').click(function(){
    $('.help').toggle("slow");
    });
    $('.feed-body-toggler').click(function(){
        var count = $(this).attr('count');
        $( ".feed-body[count^="+count+"]" ).toggle("slow");
    });
});

/**
 * http://stackoverflow.com/questions/985272/selecting-text-in-an-element-akin-to-highlighting-with-your-mouse
 */
jQuery.fn.selectText = function(){
    var doc = document
        , element = this[0]
        , range, selection
        ;
    if (doc.body.createTextRange) {
        range = document.body.createTextRange();
        range.moveToElementText(element);
        range.select();
    } else if (window.getSelection) {
        selection = window.getSelection();
        range = document.createRange();
        range.selectNodeContents(element);
        selection.removeAllRanges();
        selection.addRange(range);
    }
};

$(function() {
    $('li').click(function() {
        $(this).selectText();
    });
});